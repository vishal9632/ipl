
//first problem



function matchesPerYear(matches){
    let matches_year={};
    for(let i=0;i<matches.length;i++){ 

        if(!(matches[i].season in matches_year)){

            matches_year[matches[i].season]=1
        }
        else {
           matches_year[matches[i].season]+=1
        }
  }
    return(matches_year)
 }
//  console.log("Total no of matches"+" "+matches.length+"\n"+"matches year wise")
//  console.log(matchesPerYear(matches))

//  console.log("\n\n\n")







 //2nd problem


//no of matches won by team per year 

function winningTeamYearWise(matches){
    let winningTeamYearly={};
    for(let i=0;i<matches.length;i++){ 

        if(!(matches[i].season in winningTeamYearly)){

            winningTeamYearly[matches[i].season]=[];
            winningTeamYearly[matches[i].season].push(matches[i].winner);
        }
        else {
            winningTeamYearly[matches[i].season].push(matches[i].winner);
        }
  }
    //console.log(winningTeamYearly)
    return(winningTeamYearly)
 }




//console.log(listOfWinniingTeam[2008]);
//winning numbers of match team yearly
function WinningNoOfTeams(matches){
    let team ={}
    let listOfWinniingTeam=winningTeamYearWise(matches)
    for(let key in listOfWinniingTeam){
        team[key]={}

        //console.log(key)
        for(let i=0;i<listOfWinniingTeam[key].length;i++){

            if(!(listOfWinniingTeam[key][i] in team[key])){
                team[key][listOfWinniingTeam[key][i]]=1
            }
            else{
                team[key][listOfWinniingTeam[key][i]]+=1
            }
        }
        
    }
    //console.log(team)
    return team;
}
// console.log(WinningNoOfTeams(matches))






//3rd one 




//This function is used to find matches id played that year/season 

function matchid(matches,seasonYear){
    let arr=[];
    let k=0;
        for(let i=0;i<matches.length;i++){
            if(matches[i].season==seasonYear){
                 arr[k]=matches[i].id;
                 k++;
            }
        }
        return arr;
}
//console.log(matchid(matches,2016))


//This function is playead team in that Season
function teamPlayed(matches,seasonYear){
    let obj={}
    for(let i=0;i<matches.length;i++){
        if(matches[i].season==seasonYear){
                let key =matches[i].team1
                let key_1 =matches[i].team2
            if(!(key in obj)){
                   obj[key]=0
            }
            if(!(key_1 in obj)){
                obj[key_1]=0
         }
        }
    }
    return obj
}
//console.log(teamPlayed(matches,2017))


function extraRunByTeam(matches,seasonYear,deliveries){
    let arr=matchid(matches,seasonYear)
    let team=teamPlayed(matches,seasonYear)

    for(let i=0;i<arr.length;i++){
        for(let j=0;j<deliveries.length;j++){
            if(deliveries[j].match_id==arr[i]){
                 team[deliveries[j].batting_team]+=deliveries[j].extra_runs
            }

        }
    }
    return team
}
// console.log(extraRunByTeam(matches,2016,deliveries))  


// console.log("\n\n\n")         











/*4th one


This function is playead Bowler in that Season*/
function bowlerPlayed(matches,deliveries,seasonYear){
    let arr= matchid(matches,seasonYear)
    let obj={}
    for(let j=0;j<arr.length;j++){
        for(let i=0;i<deliveries.length;i++){
                   if(deliveries[i].match_id==arr[j]){
                       let key=deliveries[i].bowler
                        if(!(key in obj)){
                            obj[key]={run:0,
                                      ball:0}
                        }
                   }
        }
    }
    return obj
}


function bowlersRecord(matches,seasonYear,deliveries){
    let bowlerPlayed_1=bowlerPlayed(matches,deliveries,seasonYear)
    let arr = matchid(matches,seasonYear)
        



        for(let j=0;j<arr.length;j++){

             for(let i=0;i<deliveries.length;i++){


                if(deliveries[i].match_id==arr[j]){

                let key=deliveries[i].bowler;
                  if(key in bowlerPlayed_1){

                         bowlerPlayed_1[key].ball+=1
                         bowlerPlayed_1[key].run+=deliveries[i].total_runs
                   }
                }
            }

        }
    return bowlerPlayed_1
}
//console.log(bowlersRecord(matches,2016,deliveries))

function economyOfBowler(matches,seasonYear,deliveries){
    let arr=bowlersRecord(matches,seasonYear,deliveries)
    let Bowler =[]
    for(let key in arr){
        //console.log(key)
        Bowler.push([key,(((arr[key].run)/(arr[key].ball))*6).toFixed(2)])
        
    }
     //console.log(Bowler)
    return Bowler
}
//economyOfBowler(matches,2016,deliveries)

function topEconomicBowler(matches,seasonYear,deliveries,noOfTopPlayers){
    let arr=economyOfBowler(matches,seasonYear,deliveries)
    if(noOfTopPlayers>arr.length){
        return "Sorry ,Total no of players are "+arr.length
    }
    arr.sort(function(a, b) { return a[1] - b[1] })
    //console.log(obj)
    return arr.slice(0,noOfTopPlayers)
}

// console.log(topEconomicBowler(matches,2015,deliveries,10))
module.exports = { matchesPerYear ,WinningNoOfTeams,extraRunByTeam,topEconomicBowler} 