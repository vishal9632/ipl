var fs = require('fs');
const matches = require('../data/matches.json')
const deliveries = require('../data/deliveries.json')
const problems = require('./ipl.js'); 



//__________________prob-01____________________//
const matches_per_year=problems.matchesPerYear(matches)
console.log(matches_per_year)

fs.writeFile('../public/matchesPerYear.json', JSON.stringify(matches_per_year), (err) => { 
   if (err) console.log(err)
})


console.log("\n\n")



//__________________prob-02____________________//
const winning_no_of_team_year_wise=problems.WinningNoOfTeams(matches)
console.log(winning_no_of_team_year_wise)

fs.writeFile('../public/listOfWinniingTeam.json', JSON.stringify(winning_no_of_team_year_wise), (err) => { 
    if (err) console.log(err)
})


console.log("\n\n")



//__________________prob-03____________________//
const Extra_Run_by_team=problems.extraRunByTeam(matches,2016,deliveries)
console.log(Extra_Run_by_team)

fs.writeFile('../public/extraRunByTeam.json', JSON.stringify(Extra_Run_by_team), (err) => { 
    if (err) console.log(err)
})


console.log("\n\n")


//__________________prob-04____________________//
const top_Economic_bowler_in_that_year=problems.topEconomicBowler(matches,2015,deliveries,10)
console.log(top_Economic_bowler_in_that_year)

fs.writeFile('../public/topEconomicBowler.json', JSON.stringify(top_Economic_bowler_in_that_year), (err) => { 
    if (err) console.log(err)
})




//__________________Extra prob_____________//
//Total Boundaries scored by MSDhoni in 2015//

const boundaries=problems.boundaries(matches,deliveries,'MS Dhoni',2015)
console.log(boundaries)

fs.writeFile('../public/boundaries.json', JSON.stringify(boundaries), (err) => { 
    if (err) console.log(err)
})



//Total player Bowled by Bumrah in 2016//
const played_By_Bowler=problems.playedByBowler(matches,deliveries,'JJ Bumrah',2016)
console.log(played_By_Bowler)

fs.writeFile('../public/played_By_Bowler.json', JSON.stringify(played_By_Bowler), (err) => { 
    if (err) console.log(err)
})


//Total No Balls thrown by RCB in 2015//
const total_ball_By_Team=problems.totalballByTeam(matches,deliveries,'Royal Challengers Bangalore',2015)
console.log(total_ball_By_Team)

fs.writeFile('../public/total_ball_By_Team.json', JSON.stringify(total_ball_By_Team), (err) => { 
    if (err) console.log(err)
})